<?php

namespace Pumu\RamseyUuid;

use Pumu\UuidApi as Api;
use Ramsey\Uuid as Ramsey;
use Pumu\UuidApi\Uuid;

class Generator implements Api\Generator
{
    /**
     * @var Validator
     */
    private $validator;

    /**
     * Generator constructor.
     */
    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    public function random(): Uuid
    {
        return new UuidValue(Ramsey\Uuid::uuid4()->toString(), $this->validator);
    }

    public function fromString(string $uuid): Uuid
    {
        return new UuidValue($uuid, $this->validator);
    }
}
