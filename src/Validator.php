<?php

namespace Pumu\RamseyUuid;

use Ramsey\Uuid as Ramsey;
use Pumu\UuidApi as Api;


class Validator implements Api\Validator
{
    /**
     * @param string $uuid
     * @return bool
     */
    public function isValid(string $uuid): bool
    {
        return Ramsey\Uuid::isValid($uuid);
    }
}
