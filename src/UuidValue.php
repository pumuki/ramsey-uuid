<?php
namespace Pumu\RamseyUuid;

use InvalidArgumentException;
use Pumu\UuidApi\Exception\InvalidUuidException;
use Pumu\UuidApi\Uuid as UuidInterface;

class UuidValue implements UuidInterface
{
    /**
     * @var string
     */
    private $uuid;

    /**
     * @throws InvalidUuidException
     */
    public function __construct(string $uuid, Validator $validator)
    {
        if (false === $validator->isValid($uuid)) {
            throw new class("invalid uuid provided") extends InvalidArgumentException implements InvalidUuidException {};
        }

        $this->uuid = $uuid;
    }

    public function toString(): string
    {
        return $this->uuid;
    }
}